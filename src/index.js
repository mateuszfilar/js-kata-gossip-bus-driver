export function run(routes) {
  /**
   * 3 1 2 3
   * 3 2 3 1
   * 4 2 3 4 5
   */
  const gossipCount = routes.length;
  let iteratorCount = 0;
  const maxIterations = 480;
  const driversGossip = routes.map((_, index) => new Set([ index ]));

  while (!areAllGossipsKnown(driversGossip)) {
    iteratorCount++;

    if (iteratorCount === maxIterations) {
      return 'never';
    }

    const currentDriversStops = getDriversStops(routes, iteratorCount);

  }

  return iteratorCount;
}

function getDriversStops(routes, iterator) {
  return routes.map(route => route[iterator % route.length]);
}

function spreadGossips(driversGossip, currentDriversStops) {
  const currentStopsState = getCurrentStopsState(currentDriversStops);
  // TODO: iterate through current stops state and find if any contains more than 1 driver
  // if yes => exchange gossips
}

function getCurrentStopsState(currentDriversStops) {
  return currentDriversStops.reduce((currentStopsState, driverStop, driver) => ({
    ...currentStopsState,
    [driverStop]: currentStopsState[driverStop] ? [ ...currentStopsState[driverStop], driver ] : [ driver ]
  }));
}

function areAllGossipsKnown(driversGossip) {
  let length = driversGossip.length;
  return driversGossip.every(driver => driver.size === length);
}
