import { run } from './index';


test('2 busiarze mają po 1 takim samym przystanku', () => {
  const input = [
    [ 1 ],
    [ 1 ]
  ];

  expect(run(input)).toBe(1);
});

test('never', () => {
  const input = [
    [ 1 ],
    [ 2 ]
  ];

  expect(run(input)).toEqual('never');
});
